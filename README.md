<div align="center">

<img src="https://www.sunilprasad.com.np/assets/profile.png" alt="Profile Photo" width="200" height="200">

## Hello There!

### It's Sunil Prasad Regmi

A student who loves to make something useless and enjoys it. Also, I can 
make something useless to useful for daily life.

### Hosted Subdomains:

<img src="https://www.sunilprasad.com.np/assets/icons/crichd.svg" alt="CricHD Logo" width="20" height="20"> [CricHD](https://crikhd.sunilprasad.com.np) |  <img src="https://www.sunilprasad.com.np/assets/icons/daddyhd.svg" alt="DaddyHD Logo" width="20" height="20"> [DaddyHD](https://daddy.sunilprasad.com.np) |  <img src="https://www.sunilprasad.com.np/assets/icons/fancyfont.svg" alt="Fancyfont Logo" width="20" height="20">  [Fancyfont](https://fancyfont.sunilprasad.com.np) |  <img src="https://www.sunilprasad.com.np/assets/icons/horoscope.svg" alt="Horoscope Logo" width="20" height="20"> [Horoscope](https://horoscope.sunilprasad.com.np) |  <img src="https://www.sunilprasad.com.np/assets/icons/iptv.svg" alt="IPTV Logo" width="20" height="20"> [IPTV](https://iptv.sunilprasad.com.np) |  <img src="https://www.sunilprasad.com.np/assets/icons/preeti.svg" alt="Preeti Logo" width="20" height="20"> [Preeti](https://preeti.sunilprasad.com.np) 

<img src="https://www.sunilprasad.com.np/assets/icons/radio.svg" alt="Radio Logo" width="20" height="20"> [Radio](https://radio.sunilprasad.com.np) |  <img src="https://www.sunilprasad.com.np/assets/icons/sangeet.svg" alt="Sangeet Logo" width="20" height="20"> [Sangeet](https://sangeet.sunilprasad.com.np) |  <img src="https://www.sunilprasad.com.np/assets/icons/weather.svg" alt="Weather Logo" width="20" height="20"> [Weather](https://weather.sunilprasad.com.np) 

### See more projects at:

<img src="https://www.sunilprasad.com.np/assets/icons/github.svg" alt="GitHub Logo" width="20" height="20"> [Github](https://github.com/SunilPrasadRegmi) |  <img src="https://www.svgrepo.com/download/360454/gitlab.svg" alt="GitLab Logo" width="20" height="20"> [Gitlab](https://gitlab.com/prasunil058) 

### Download CV  | Contact

[Download CV](https://www.sunilprasad.com.np/assets/cv/1.pdf)  |  [Contact](https://t.me/guruusr/)


</div>
